This week's code just highlights lines. Still working on it. Got a lot of parameters that changing them can change the output image, but in general, can highlights lines that we need more clearly and I think it is working better than contour functions. It detects lines instead of edges

For that, I divided the image into channels, which makes more clear output image (Lines are better highlighted) than just using "HoughLinesP" function of opencv.

I'm looking at dominant color at the moment.
