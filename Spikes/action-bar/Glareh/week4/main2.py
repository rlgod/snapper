'''
Created on Aug 30, 2013

@author: sm
'''
import cv2
import numpy as np
import math
# import Image
# from operator import itemgetter, attrgetter

# color detection
# bounding boxes
# look at pixel #5

class color:
        count = 0
        R = 0
        G = 0
        B = 0
        def __init__(self,Blue,Green,Red):
            self.count = self.count + 1
            self.R = Red
            self.G = Green
            self.B = Blue
        def AskRGB(self):
            return self.R, self.G , self.B
        def Howmany(self):
            return self.count
        def Addit(self):
            self.count +=1
            return


def main():
    inim = cv2.imread("foursquare.png")#"Screen.jpg")#
#     cv2.namedWindow("Result", cv2.CV_WINDOW_AUTOSIZE)
#     res1 = func18(inim)
#     res2 = func20(res1)
    res2 = func22(inim)
    cv2.imshow("Result",res2)
    #func20()
    cv2.waitKey(0)
    return

def func22(image):
    # deviding by chanel is better and more common than getting the whole image and using it.
    # This piece is divided in channels and then the lines where found in each channel, all mapped in one original image
    img = image.copy()
    cv2.imshow("edge",image)

    blue, green, red = cv2.split(img)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     blurred= cv2.GaussianBlur(gray,(1,1),0)#gray,(5,5),0) 
    edges = cv2.Canny(gray,0,1)#(blurred, 80, 120)
    blueedges = cv2.Canny(blue, 0, 1)
    greenedges = cv2.Canny(green, 0, 1)
    rededges = cv2.Canny(red, 0, 1)
    
    lines = cv2.HoughLinesP(blueedges, 1, math.pi/2, 2, None, 30, 1);
    for line in lines[0]:
        pt1 = (line[0],line[1])
        pt2 = (line[2],line[3])
        cv2.line(img, pt1, pt2, (0,0,255), 3)
    lines = cv2.HoughLinesP(greenedges, 1, math.pi/2, 2, None, 30, 1);
    for line in lines[0]:
        pt1 = (line[0],line[1])
        pt2 = (line[2],line[3])
        cv2.line(img, pt1, pt2, (0,0,255), 3)
    lines = cv2.HoughLinesP(rededges, 1, math.pi/2, 2, None, 30, 1);
    for line in lines[0]:
        pt1 = (line[0],line[1])
        pt2 = (line[2],line[3])
        cv2.line(img, pt1, pt2, (0,0,255), 3)
    lines = cv2.HoughLinesP(edges, 1, math.pi/2, 2, None, 30, 1);
    for line in lines[0]:
        pt1 = (line[0],line[1])
        pt2 = (line[2],line[3])
        cv2.line(img, pt1, pt2, (0,0,255), 3)
    
    return img

def func21(img): # grouping boxes... Apparently
    # Split out each channel
    blue, green, red = cv2.split(img)
    
    def medianCanny(img, thresh1, thresh2):
        median = np.median(img)
        img = cv2.Canny(img, int(thresh1 * median), int(thresh2 * median))
        return img
    
    # Run canny edge detection on each channel
    blue_edges = medianCanny(blue, 0.2, 0.3)
    green_edges = medianCanny(green, 0.2, 0.3)
    red_edges = medianCanny(red, 0.2, 0.3)
    
    # Join edges back into image
    edges = blue_edges | green_edges | red_edges
    
    # Find the contours
    contours,hierarchy = cv2.findContours(edges, cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    
    hierarchy = hierarchy[0] # get the actual inner list of hierarchy descriptions
    
    # For each contour, find the bounding rectangle and draw it
    for component in zip(contours, hierarchy):
        currentContour = component[0]
        currentHierarchy = component[1]
        x,y,w,h = cv2.boundingRect(currentContour)
        if currentHierarchy[2] < 0:
            # these are the innermost child components
            cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),3)
        elif currentHierarchy[3] < 0:
            # these are the outermost parent components
            cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),3)
    
    # Finally show the image
    cv2.imshow('img',img)
    return img

def func20(img):# Generally gets the lines only
#     img = cv2.imread("dropbox.png") #"Screen.jpg")
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #blurred= cv2.GaussianBlur(gray,(7,7),0) # 5 is a better threshold
    blurred= cv2.GaussianBlur(gray,(1,1),0)#gray,(5,5),0) 
    edges = cv2.Canny(blurred, 80, 120)
    lines = cv2.HoughLinesP(edges, 1, math.pi/2, 2, None, 30, 1);
    for line in lines[0]:
        pt1 = (line[0],line[1])
        pt2 = (line[2],line[3])
        cv2.line(img, pt1, pt2, (0,0,255), 3)
#     cv2.imshow("res",img)
    return img
    
def func18(image): # finding contours... Apparently :P
    img2= cv2.GaussianBlur(image,(5,5),0)
    img = cv2.Canny(img2, 100, 200)

    im=image.copy()
    #imgray = cv2.cvtColor(im,cv2.COLOR_BGR2GRAY)

    ret,thresh = cv2.threshold(img,127,255,0)
    contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

    # Just because I can! :P
    print len(contours)
    cnt = contours[0]
    print len(cnt)
    
    x,y,w,h = cv2.boundingRect(cnt)
    cv2.rectangle(im,(x,y),(x+w,y+h),(255,0,213),2)
    
    cv2.drawContours(im,contours,-1,(0,145,255),3)
    cv2.drawContours(im,contours,-1,(0,255,0),-1)
    cv2.drawContours(im,[cnt],0,(255,0,0),-1)
    for h,cnt in enumerate(contours):
        mask = np.zeros(img.shape,np.uint8)
        cv2.drawContours(mask,[cnt],0,255,-1)
        mean = cv2.mean(im,mask = mask)
    cv2.imshow("IM",im)
    cv2.imshow("IM2",img)
    return im
    
def func16(image): 
# List the colors in the image... Apparently. I haven't checked the results to make sure yet.

    colorlist=[]
    image2=cv2.cv.fromarray(image)
    w, h = cv2.cv.GetSize(image2)
    print w, h
            
    firstColor = color(image[0,0,0],image[0,0,1],image[0,0,2])
    colorlist.append(firstColor)
    print firstColor.AskRGB()
    
    for i in range (0,h):
        for j in range (0, w):
            newColor = color(image[i,j,0],image[i,j,1],image[i,j,2])
            ck=0
            for k in colorlist: 
                if newColor.AskRGB()== k.AskRGB():
                    k.Addit()
                    ck=1
            if ck==0:
                colorlist.append(newColor)

    colorlist.sort(key=lambda x: x.count, reverse=True)
    print len(colorlist)
    for m in range (0,len(colorlist)):
        print colorlist[m].AskRGB(),colorlist[m].Howmany()
    return image
 
def func12(image): # just canny. Needs to be modified.
    img2= cv2.GaussianBlur(image,(5,5),0)
    img = cv2.Canny(img2, 100, 200)
    return img


if __name__ == '__main__':
    main()



