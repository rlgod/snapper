import cv2, sys
import cv2.cv as cv
import numpy as np

#Barebones
def retImageColor(_img):
	_size = cv.GetSize(cv.fromarray(_img))
	_height = _size[1]
	_width = _size[0]
	_hues = [0,0,0]
	for i in range(_height):
		_hues[0] += _img[i,1][0]
		_hues[1] += _img[i,1][1]
		_hues[2] += _img[i,1][2]
	for i in xrange(3):
			_hues[i] = _hues[i] / _height
			
	_result = _hues[2], _hues[1], _hues[0]
	return _result
		
		
if __name__ == "__main__":
	image = cv2.imread(sys.argv[1])
	print retImageColor(image)
