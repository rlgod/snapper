import cv2, sys
import cv2.cv as cv
import numpy as np

#imgEdges = cv2.Canny(self.imgBlur, 100, 300, 3)
LOWER_THRESHOLD_MAX = 100

"""
options = {	'-c': ColorAnalysis,
			'-ca': ActionBarColor,
			'-cb': BackgroundColor,
			'-bb': BoundingBoxes,
}"""


#image class
#default threshold is 100
class image:
	def __init__(self, image, threshold = None):
		self.img = cv2.imread(image)
		
		self.size = cv.GetSize(cv.fromarray(self.img))
		self.height = self.size[0]
		self.width = self.size[1]
		
		self.imgBlur = cv2.GaussianBlur(self.img, (3,3), 0)
		self.imgHSV = cv2.cvtColor(self.img, cv.CV_BGR2HSV)
		self.imgGray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
		
		if threshold is None:
			self.setEdges()
		else:
			self.setEdges(threshold)
			
	def setEdges(self, threshold = None):
		if threshold is None:
			self.imgEdges = cv2.Canny(self.imgBlur, 100, 300, 3)
		else:
			self.imgEdges = cv2.Canny(self.imgBlur, threshold, threshold*3, 3)
			
		self.setContours()
		self.setBoundingBoxes()
			
	def setContours(self):
		self.imgContours = cv2.findContours(self.imgEdges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE, (0,0))[0]
		
	def setBoundingBoxes(self):
		self.boundingBoxes = []
		for i in xrange(0, len(self.imgContours)):
			self.boundingBoxes.append(cv2.boundingRect(self.imgContours[i]))
		
#May not be required
class imageWindow:
	def __init__(self, name, image):
		self._image = image
		self._name = name
		cv2.namedWindow(name, cv2.CV_WINDOW_AUTOSIZE)
		
	def renderImage(self):
		cv2.imshow(self._name, self._image.img)
		
	def renderEdges(self):
		cv2.imshow(self._name, self._image.imgEdges)
	
	"""def renderBoundingBoxes(self):
		for i in range(len(img.imgContours)):
			x, y, w, h = img.boundingBoxes[i]
			cv2.rectangle(img.img,(x,y),(x+w,y+h),(0,255,0),2)"""
		
		
class actionBarDetector:
	def __init__(self, img):
		self._img = img
	#Detect top section with edges 
	#TODO detect with bounding boxes
	def detectTopSection(self):
		i = 0
		while i < self._img.size[1]:
			i += 1
			if self._img.imgEdges[i][0] > 0 and i > 5:
				return i

	def pixelMethod():
		pass
	

class backgroundDetector:
	pass
	
def main():
	if len(sys.argv) < 2:
		print "\nUsage: python main.py image.ext -function\n\n functions:\n\t-c: Color Analysis\n\t-ca: ActionBar Color\n\t-cb: Background Color\n\t-bb: Bounding Boxes\n"
		return 0
	
	img = image(sys.argv[1], 100)

	imgWindow = imageWindow("main", img)
	#print img.boundingBoxes
	for i in range(len(img.boundingBoxes)):
		x, y, w, h = img.boundingBoxes[i]
		"""print img.boundingBoxes[i][1]
		print img.boundingBoxes[i][3]"""
		cv2.rectangle(img.img,(x,y),(x+w,y+h),(0,150,0),3)
	while(1):
		imgWindow.renderImage()
		
		if cv.WaitKey(0):
			break
	#print img.boundingBoxes
	
if __name__ == "__main__":
	main()
	
