import cv2, sys
import cv2.cv as cv
import numpy as np
import math

#THIS WILL TAKE IN AN IMAGE AS A SINGLE ARGUMENT
#THE IMAGE MUST BE A CROPPED ELEMENT OF A UI
#E.G. ACTION BAR, MENU LIST, ETC.

#TODO
#make more modular
#cleanup

class colorDetector:
	def __init__(self, image, x = None, y = None, w = None, h = None):
		#If coordinates are specified, will initialize with those coordinates in mind
		if x and y and w and h is None:
			self.init(image)
		else:
			self.init(image, x, y, w, h)
		
		if self.gradient is True:
			self.setGradient(1)
		else:
			self.setMeanColor(1)
	
	#initializes the starting variables
	#if coordinates are defined, will set the image to the cropped area
	def init(self, image,  x = None, y = None, w = None, h = None):
		if x and y and w and h is None:
			self.img = cv2.imread(image)
		else:
			self.img = cv2.imread(image)
			self.img = self.img[x:y, w:h]
						
		self.size = cv.GetSize(cv.fromarray(self.img))
		self.height = self.size[0]
		self.width = self.size[1]
		
		self.imgBlur = cv2.GaussianBlur(self.img, (3,3), 0)
		self.imgHSV = cv2.cvtColor(self.img, cv.CV_BGR2HSV)
		self.imgGray = cv2.cvtColor(self.img, cv2.COLOR_BGR2GRAY)
	
		self.setEdges()
		self.setImageRange()
		self.checkGradient(1)
	
	def setEdges(self, threshold = None):
		self.imgEdges = cv2.Canny(self.imgBlur, 100, 300, 3)	
		self.setContours()
		self.setBoundingBoxes()
			
	def setContours(self):
		self.imgContours = cv2.findContours(self.imgEdges, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE, (0,0))[0]
		
	def setBoundingBoxes(self):
		self.boundingBoxes = []
		for i in xrange(0, len(self.imgContours)):
			self.boundingBoxes.append(cv2.boundingRect(self.imgContours[i]))
			
	#If color values differ by a certain threshold, will return true
	def checkGradient(self, j):
		_low = self.imgGray[0,j]
		_high = self.imgGray[0,j]
		
		for i in range(self.height):
			if self.imgGray[i,j] > _high:
				_high = self.imgHSV[i,j][0]
			if self.imgGray[i,j] < _low:
				_low = self.imgHSV[i,j][0]
		if math.fabs(_high - _low) > 40:
			self.gradient = True
		else: 
			self.gradient = False
		
	#Defines the upper and lower bounds of a simple gradient	
	def setGradient(self,j):
		_low = self.imgHSV[0,j][0]
		_high = self.imgHSV[0,j][0]
		
		_lowCoord = 0
		_highCoord = 0		
		for i in range(self.height):
			if self.imgHSV[i,j][0] > _high:
				_high = self.imgHSV[i,j][0]
				_highCoord = i
			if self.imgHSV[i,j][0] < _low:
				_low = self.imgHSV[i,j][0]
				_lowCoord = i
		
		self.highGrad = self.img[_highCoord,j]
		self.lowGrad = self.img[_lowCoord,j]

		
	#Defines valid areas to check
	#Will set up an array of size img.size
	#Marks every pixel contained within a bounding box as 0, else 1
	#Should theoretically mark all elements (such as buttons, etc.) as invalid
	def setImageRange(self):
		self.imageRange = np.ones((self.height, self.width))
		
		#so many nested loops
		for i in range(len(self.boundingBoxes)):
			x, y, w, h = self.boundingBoxes[i]
			if w < 0.4*self.width or h < 0.4*self.height:
				for j in range(x, x+w):
					for k in range(y, y+h):
						self.imageRange[j,k] = 0	 		
	
	#Returns the mean colour for a column "j"
	def setMeanColor(self, j):
		if j < 0 or j >= width:
			return False
		_hues = []
		for i in range(self.height):
			_hues[0] += self.img[i,j][0]
			_hues[1] += self.img[i,j][1]
			_hues[2] += self.img[i,j][2]
		for i in range(3):
			_hues[i] /= self.height
		self.meanHues = _hues
		
		
	#Will return a colour from row, EXLUDING areas bound off
	def setMeanColorRangeFilter(self, j):
		if j < 0 or j >= width:
			return False
		_hues = []
		for i in range(self.height):
			if self.imgRange[i,j] == 1:
				_hues[0] += self.img[i,j][0]
				_hues[1] += self.img[i,j][1]
				_hues[2] += self.img[i,j][2]
		for i in range(3):
			_hues /= self.height
		return _hues
		
	#Takes the filtered colors of the column first pixel in, and last pixel in and middle
	#returns the mean colors
	def meanColorRange(self):
		_hues = []
		_hues += self.meanColorRangeFilter(1)
		_hues += self.meanColorRangeFilter(self.width/2)
		_hues += self.meanColorRangeFilter(self.width-1)
		
		_hues /= 3
		return _hues
	
	#Will return a simple two-tone gradient at both peaks for a column/row
	def simpleGradient(self, Threshold = None):
		pass
		
#DEFINING GRADIENT MAPS
#0 = TROUGH
#1 = PEAK
#2 = RISE
#3 = DROP

#TODO extrapolate rate of change equations

#All of these values are on the assumption of segregation by value/brightness
	def detectGradientMaps(self, j):
		pass
		
	
#HSV Value with openCV is simply a maximum value
#Brightness is a better measure for human perception
#REMEMBER OpenCV works in BGR
def brightness(R,G,B):
	return ((0.2126*R) + (0.7152*G) + (0.0722*B))
	
def main():
	#Initialize image object
	img = colorDetector(sys.argv[1])
	
	if img.gradient is True:
		print img.highGrad
		print img.lowGrad
	else:
		print img.meanHues
	
	for i in range(img.height):
		print img.imgGray[i][0]
	
if __name__ == "__main__":
	main()
