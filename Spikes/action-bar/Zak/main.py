import sys 			# System bindings
import numpy as np
import scipy.stats as stats
import cv2 			# OpenCV bindings


class EdgeDetector(object):
	def __init__(self, imageLoc ):
		self.src = cv2.imread(imageLoc, 1) 
		self.grey_src = cv2.cvtColor(self.src,cv2.COLOR_BGR2GRAY)

		if (self.src == None):
			print "No image data. Check image location for typos"
			exit()
		else:
			cv2.imshow("Before", self.graysrc)
			print "Image loaded successfully"

	def bounding_box(self):
		contours = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE,(0,0))[1]

		for i in xrange(0, len(contours) ):
			x, y, w, h = cv2.boundingRect(contours[i])

			cv2.rectangle(grey_src,(x,y),(x+w,y+h),(0,255,0),2)

		cv2.imshow("Bounding Box", grey_src)
  		#print contours
    	cv2.waitKey(0)	

def main(imageLoc):
	
	edge_det = EdgeDetector(imageLoc)
	edge_det.bounding_box()

if __name__ == "__main__":
	if (len(sys.argv) != 2):			# Checks if image was given as cli argument
		print "error: syntax is 'python main.py /example/image/location.jpg'"
	else:
		main(sys.argv[1])				# Pass the image location to __main__