import sys                      # System bindings
import cv2                      # OpenCV bindings
import numpy as np
import ssdmodules.colour as ssdcolour		# SSD colour detection methods
import ssdmodules.region_detection as ssdregion

if __name__ == "__main__":
    if len(sys.argv) != 2:                        # Checks if image was given as cli argument
        print "error: syntax is 'python main.py /example/image/location.jpg'"
    else:
        src = cv2.imread(sys.argv[1], 1)
        ssdcolour.background_colour(src, 1, 0)		## (image <ndarray>, Gradient or Average, Debug Enable)
        ssdregion.detect_regions(src, 0, 1)			## (image <ndarray>, blur_method(medianBlur, Gaussian, Blur), show_images<1 or 0>)