##@package region_detection
# Region detection techniques
import sys
import cv2
import numpy as np

## Region Detect
# Parameters:
#	image -> Source Image
#	blur_type -> medianBlur(0), GaussianBlur(1), Blur(2)
def detect_regions(image, blur_type, show_regions):
	Detector = RegionDetector(image, blur_type)
	Detector.thresh_callback()
	Detector.get_areas(show_regions)

class RegionDetector():
	def __init__(self, image, blur_type):
		self.src = image
		
		self.h, self.w, self.c = self.src.shape
		self.greenborders = np.zeros((self.h+10, self.w+10, 3),np.uint8)
		self.greenborders[::] = (0,255,0)
		for y in xrange (5, self.h):
			for x in xrange(5, self.w):
				self.greenborders[y][x] = self.src[y][x]
		cv2.imshow("image", self.src)
		cv2.waitKey(0)

		# self.graysrc = cv2.cvtColor( self.src, cv2.COLOR_BGR2HSV )
		self.graysrc = cv2.cvtColor( self.greenborders, cv2.COLOR_BGR2GRAY )
		try:
			if blur_type == 0:
				self.graysrc = cv2.medianBlur( self.greenborders, 7)
			elif blur_type == 1:
				self.graysrc = cv2.GaussianBlur( self.greenborders, (3,3), 0)
			elif blur_type == 2:
				self.graysrc = cv2.Blur( self.greenborders, 5)
		except(RuntimeError):
			print "No blur type given"
		self.thresh = 100
		self.Rectangles = []
		self.colors_count = {}

		if (self.src == None):
			print "No image data. Check image location for typos"
			exit()
		else:
			cv2.imshow("Before", self.graysrc)
			print "Image loaded successfully"

	def get_areas(self, show_regions):
		print "Detecting Regions"
		localRectangles = []
		myImages = []
		for x in xrange(0, len(self.Rectangles)):
			if ((float(self.Rectangles[x][2])/float(self.w)) > 0.4) and ((float(self.Rectangles[x][3])/float(self.h)) > 0.01):
				localRectangles.append(self.Rectangles[x])

		for rects in xrange(0, len(localRectangles)):
			x, y, w, h = localRectangles[rects]
			myImages.append(self.src[y:y+h, x:x+w])

		if show_regions == 1:
			for image in xrange(0, len(myImages)):
				cv2.imshow("Image", myImages[image])
				cv2.waitKey(0)

	def thresh_callback(self):
		#result, threshold_output = cv2.threshold( self.graysrc, self.thresh, 255, cv2.THRESH_BINARY )			# Threshold edge detection
		#result, threshold_output = cv2.threshold(self.graysrc, 150, 255, cv2.THRESH_BINARY_INV);
		threshold_output = cv2.Canny(self.graysrc, 5, 35)													# Canny edge detection
		contours, hierarchy = cv2.findContours(threshold_output, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE,(0,0))
		contour_image = self.src.copy();
		Classifier = cv2.CascadeClassifier()
		for i in xrange(0, len(contours)):
			cnt = contours[i]
			#cnt = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)		# Applies contour approximation to try and ignore the curves (doesn't work so well though)
			self.Rectangles.append(cv2.boundingRect(cnt))
		grouped_rectangles = cv2.groupRectangles(self.Rectangles, 0, 2)
		# print "Grouped Rectangles: ", grouped_rectangles

		for i in xrange(0, len(grouped_rectangles[0])):
			x,y,w,h = grouped_rectangles[0][i]
			cv2.rectangle(contour_image,(x,y),(x+w,y+h),(0,255,0),2)

		print "Found contours"
		cv2.namedWindow("Contours", cv2.WINDOW_AUTOSIZE)
		cv2.imshow("Contours", self.src)
		cv2.imshow("Contour_image", threshold_output)