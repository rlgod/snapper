import xml.etree.ElementTree as ET
import cv2

def RGB2HEX(color):
	return '#%02x%02x%02x' % color

class AndroidLinearElement(ET.Element):
	def __init__(self, region_rectangle, margin_left, margin_top, background_color):
		attributes = {}
		self.region_rectangle = region_rectangle
		self.width = self.region_rectangle[2] # Will be used later for calculating absolute positions of elements on the page
		attributes['{http://schemas.android.com/apk/res/android}layout_width'] = (str(self.width) + 'dp')
		self.height = self.region_rectangle[3] # Will be used later for calculating absolute positions of elements on the page
		attributes['{http://schemas.android.com/apk/res/android}layout_height'] = (str(self.height) + 'dp')
		attributes['{http://schemas.android.com/apk/res/android}layout_marginTop'] = (str(margin_top) + 'dp')
		attributes['{http://schemas.android.com/apk/res/android}layout_marginLeft'] = (str(margin_left) + 'dp')

		self.background_color = RGB2HEX(background_color)
		attributes['{http://schemas.android.com/apk/res/android}background'] = self.background_color

		super(AndroidLinearElement, self).__init__('LinearLayout', attributes)
		