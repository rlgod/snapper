##@package ssdcolour
# Method Authors listed above the respective method
# 

import sys                      # System bindings
import cv2                      # OpenCV bindings
import numpy as np
from collections import Counter

## Detect and returns the background color in an image using the method specified
# Methods: 0 = Gradient detection, 1 = Average color (0 default)
def background_colour(region_image, method, debug_enable):
	background_detector = Background(region_image)		## Class holding current image region for detection
	if method == 1:
		background_detector.twenty_most_common(debug_enable)
		color = background_detector.detect_average()
		return color	
	else:
		print "Gradient Detection not implemented yet."
		sys.exit(0)

## Background Color Detection
# Methods related to background color detection
class Background():
	## Background Class Initialisation
	def __init__(self, image_region):
		self.img = image_region							## Loads the image to analyse as a member variable
		self.manual_count = {}							## Initialises an empty dictionary for counting colours in format key:value (rgb(tuple): frequency(int))
		self.w, self.h, self.channels = self.img.shape	## Extract image dimensions from the shape of the source image for later use
		self.total_pixels = self.w*self.h				## Total pixels in source image as int

	## Count frequencies of colours and store in a dictionary as (r,g,b):(int)
	def count(self):
		for y in xrange(0, self.h):
			for x in xrange(0, self.w):
				RGB = (self.img[x,y,2],self.img[x,y,1],self.img[x,y,0])
				if RGB in self.manual_count:
					self.manual_count[RGB] += 1
				else:
					self.manual_count[RGB] = 1

	## Find the average color
	def average_colour(self):
		red = 0; green = 0; blue = 0;
		sample = 5
		for top in xrange(0, sample):
			red += self.number_counter[top][0][0]
			green += self.number_counter[top][0][1]
			blue += self.number_counter[top][0][2]

		average_red = red / sample
		average_green = green / sample
		average_blue = blue / sample
		return (average_red, average_green, average_blue,)

	## Prints the 20 most common colours in the image for debug purposes
	def twenty_most_common(self, debug_enable):
		self.count()
		self.number_counter = Counter(self.manual_count).most_common(20)
		if debug_enable == 1:
			for rgb, value in self.number_counter:
				print rgb, value, ((float(value)/self.total_pixels)*100)

	## Detects the background using average color method
	# Gradient detection is the preferred method to this (Set method 1)
	def detect_average(self):
		self.percentage_of_first = (float(self.number_counter[0][1])/self.total_pixels)
		print self.percentage_of_first
		if self.percentage_of_first > 0.8:
			return self.number_counter[0][0]
		else:
			return self.average_colour()

