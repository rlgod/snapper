##@package region_detection
# Region detection techniques
import sys
import cv2
import numpy as np
import xml.etree.ElementTree as ET
import AndroidLinearElement as ALE
import color_detection as color

def convert2dp(lRectangles, image_shape):
	print "shape: ", image_shape
	newRectangles=[]
	old_x_range = image_shape[1]
	print "oldx: ",old_x_range
	old_y_range = image_shape[0]
	print "oldy: ",old_y_range
	new_x_range = 599
	new_y_range = 829
	for i in xrange(len(lRectangles)):
		y,x,w,h = lRectangles[i]

		scaled_x = (x * new_x_range)/old_x_range
		scaled_y = (y * new_y_range)/old_y_range
		scaled_w = (w * new_x_range)/old_x_range
		scaled_h = (h * new_y_range)/old_y_range

		newRectangles.append((scaled_x, scaled_y, scaled_w, scaled_h))
	return newRectangles

## Region Detect
# Parameters:
#	image -> Source Image
#	blur_type -> medianBlur(0), GaussianBlur(1), Blur(2)

def detect_regions(image, blur_type, show_regions, tree_root):

	Detector = RegionDetector(image, blur_type)
	Detector.get_rectangles()
	Rectangles = Detector.get_areas(show_regions)
	dpRectangles = convert2dp(Rectangles, Detector.src.shape)

	for i in xrange(0, len(Rectangles)):
		x, y, w, h = Rectangles[i]
		## (AndroidLinearElement expects Rectangle, top-margin, left-margin, color)
		rect_background = color.retImageColor(Detector.src[y:y+h, x:x+w])
		print "Rect Background ", rect_background
		current_element = ALE.AndroidLinearElement(dpRectangles[i], dpRectangles[i][1], dpRectangles[i][0], rect_background)
		tree_root.append(current_element)

	return tree_root

class RegionDetector():
	def __init__(self, image, blur_type):
		self.src = image

		## Calculating some Dimensions
		self.h, self.w, self.c = self.src.shape
		self.image_area = self.w * self.h

		## Adding a bright green border to the image to allow for detection of up-to-the-edge regions
		self.greenborders = np.zeros((self.h+10, self.w+10, 3),np.uint8)
		self.greenborders[::] = (0,255,0)
		for y in xrange (5, self.h):
			for x in xrange(5, self.w):
				self.greenborders[y][x] = self.src[y][x]

		# self.graysrc = cv2.cvtColor( self.src, cv2.COLOR_BGR2HSV )
		self.graysrc = cv2.cvtColor( self.greenborders, cv2.COLOR_BGR2GRAY )
		try:
			if blur_type == 0:
				self.graysrc = cv2.medianBlur( self.greenborders, 7)
			elif blur_type == 1:
				self.graysrc = cv2.GaussianBlur( self.greenborders, (3,3), 0)
			elif blur_type == 2:
				self.graysrc = cv2.Blur( self.greenborders, 5)
		except(RuntimeError):
			print "No blur type given"
		self.thresh = 100

		self.Rectangles = []
		self.colors_count = {}

		if (self.src == None):
			print "No image data. Check image location for typos"
			sys.exit(1)
		else:
			print "Image loaded successfully"

	def get_areas(self, show_regions):

		print "Detecting Regions"
		localRectangles = []
		myImages = []
		for x in xrange(0, len(self.Rectangles)):
			if ((float(self.Rectangles[x][2])/float(self.w)) > 0.25) and ((float(self.Rectangles[x][3])/float(self.h)) > 0.01):
				if not ((float(self.Rectangles[x][2]) * float(self.Rectangles[x][3])) / self.image_area > 0.96):
					localRectangles.append(self.Rectangles[x])

		## Debug code to display cropped regions (requires show_regions to be 1)
		if show_regions == 1:
			for rects in xrange(0, len(localRectangles)):
				x, y, w, h = localRectangles[rects]
				myImages.append(self.src[y:y+h, x:x+w])

			for image in xrange(0, len(myImages)):
				cv2.imshow("Image", myImages[image])
				cv2.waitKey(0)

		return localRectangles

	def get_rectangles(self):
		#result, threshold_output = cv2.threshold( self.graysrc, self.thresh, 255, cv2.THRESH_BINARY )			# Threshold edge detection
		#result, threshold_output = cv2.threshold(self.graysrc, 150, 255, cv2.THRESH_BINARY_INV);
		threshold_output = cv2.Canny(self.graysrc, 5, 35)													# Canny edge detection
		contours, hierarchy = cv2.findContours(threshold_output, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE,(0,0))
		contour_image = self.src.copy();
		Classifier = cv2.CascadeClassifier()
		for i in xrange(0, len(contours)):
			cnt = contours[i]
			#cnt = cv2.approxPolyDP(cnt,0.1*cv2.arcLength(cnt,True),True)		# Applies contour approximation to try and ignore the curves (doesn't work so well though)
			self.Rectangles.append(cv2.boundingRect(cnt))
		grouped_rectangles = cv2.groupRectangles(self.Rectangles, 0, 2)
		# print "Grouped Rectangles: ", grouped_rectangles

		for i in xrange(0, len(grouped_rectangles[0])):
			x,y,w,h = grouped_rectangles[0][i]
			cv2.rectangle(contour_image,(x,y),(x+w,y+h),(0,255,0),2)

		print "Found contours"
		cv2.namedWindow("Contours", cv2.WINDOW_AUTOSIZE)
		cv2.imshow("Contours", self.src)
		cv2.imshow("Contour_image", threshold_output)