import cv2, sys
import numpy as np

#Barebones
def retImageColor(_img):
	_height, _width, _channels = _img.shape
	_hues = [0,0,0]
	for i in range(_height):
		_hues[0] += _img[i,1][0]
		_hues[1] += _img[i,1][1]
		_hues[2] += _img[i,1][2]
	for i in xrange(3):
			_hues[i] = _hues[i] / _height
			
	_result = _hues[2], _hues[1], _hues[0]
	return _result