import cv2
import sys
import ssdmodules.colour as ssdcolour		# SSD colour detection methods
import ssdmodules.region_detection as ssdregion
import xml.etree.ElementTree as ET
import xml.dom.minidom as minidom
import ssdmodules.AndroidLinearElement as ALE
import os

def prettify_xml(working_directory):
	file_name = working_directory + 'activity_main.xml'
	ugly_xml = minidom.parse(working_directory + 'activity_main.xml')
	pretty_xml = ugly_xml.toprettyxml()
	print type(pretty_xml)
	file_handler = open(file_name, 'wb')
	file_handler.write(pretty_xml)
	file_handler.close()

def write_xml_activity(xml_tree, working_directory):
	layout_directory = working_directory + 'res/layout/'
	if not os.path.exists(layout_directory):
		os.makedirs(layout_directory)
	xml_tree.write(layout_directory + 'activity_main.xml', 'utf-8')
	prettify_xml(layout_directory)

def write_xml_style(xml_tree, working_directory):
	style_directory = working_directory + 'res/values/'
	if not os.path.exists(layout_directory):
		os.makedirs(layout_directory)
	xml_tree.write(layout_directory + 'style.xml', 'utf-8')

class OutputStructure():
	def __init__(self, image_directory):
		self.root_element = ET.Element('RelativeLayout',{'{http://schemas.android.com/apk/res/android}layout_width': 'match_parent', '{http://schemas.android.com/apk/res/android}layout_height': 'match_parent', '{http://schemas.android.com/tools}context':'.MainActivity'})
		self.src_image = cv2.imread(image_directory, 1)

	def set_namespaces(self):
		ET.register_namespace('android', 'http://schemas.android.com/apk/res/android')
		ET.register_namespace('tools', 'http://schemas.android.com/tools')

	def main(self):
		self.set_namespaces()
		self.root_element = ssdregion.detect_regions(self.src_image, 0, 0, self.root_element)
		self.create_tree()
			
	def create_tree(self):
		self.main_activity_tree = ET.ElementTree(self.root_element)

	def give_details(self):
		root_element = self.main_activity_tree.getroot()
		print root_element.tag, root_element.attrib

if __name__ == "__main__":
	if len(sys.argv) < 3:
		print "Error no file supplied"
		sys.exit(0)									## Returns 0 if failed to run
	else:
		file_location = sys.argv[1]					## Input file directory
		working_directory = sys.argv[2]				## Root of output directory

		## Main Code Entry location
		output_structure = OutputStructure(sys.argv[1])
		output_structure.main()

		write_xml_activity(output_structure.main_activity_tree, working_directory)

        # ssdcolour.background_colour(src,1,0)		## (image <ndarray>, Gradient or Average, Debug Enable)
        # ssdregion.detect_regions(src, 0, 1)			## (image <ndarray>, blur_method(medianBlur, Gaussian, Blur), show_images<1 or 0>)

		sys.exit(1)									## Returns 1 if successfull 